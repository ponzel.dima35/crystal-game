import {
  FC,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
} from "react";
import {
  Application,
  BlurFilter,
  Container,
  FederatedPointerEvent,
  Graphics,
  Text,
} from "pixi.js";
// import { Sprite, render } from "@pixi/react";
import "./GamePage.css";
import {
  WATER_HEIGHT,
  status,
  figures,
  setPause,
  setPlay,
  endGame,
  setPending,
  selectFigure,
  deselectFigure,
  moveSelectedFigure,
  FigureData,
  onActionDone,
  field,
  FiledCellData,
} from "./useGame";
import { Moment } from "moment";
import moment from "moment";

export const GamePage: FC = () => {
  const app = useRef<Application | null>(null);
  const gameField = useRef<Graphics | null>(null);
  const figuresObjects = useRef<Record<string, Graphics>>({});
  const cellsObjects = useRef<Record<string, Graphics>>({});

  const getCellId = (cellData: FiledCellData): string => {
    return `${cellData.horizontalPosition}-${cellData.verticalPosition}`;
  };

  const getOrCreateCell = (cellData: FiledCellData) => {
    let cell = cellsObjects.current[getCellId(cellData)];
    if (!cell) {
      cell = new Graphics();
      cell.beginFill("blue", 0.1);
      cell.drawRect(
        field.x + (cellData.horizontalPosition - 1) * 40,
        field.y + (cellData.verticalPosition - 1) * 40,
        40,
        40
      );

      cell.eventMode = "static";
      cell.cursor = "pointer";
      // figure.addListener("pointerdown", () => {
      //   selectFigure(id);
      // });
      // figure.addListener("pointerup", () => {
      //   deselectFigure(id);
      // });

      gameField.current!.addChild(cell as any);
      cellsObjects.current[getCellId(cellData)] = cell;
    }

    return cell;
  };

  const getOrCreateFigure = (id: string) => {
    let figure = figuresObjects.current[id];
    if (!figure) {
      figure = new Graphics();
      figure.beginFill(0, 1);
      figure.drawRect(0, 0, 40, 40);

      figure.eventMode = "static";
      figure.cursor = "pointer";
      figure.addListener("pointerdown", () => {
        selectFigure(id);
        console.log("selectFigure");
      });
      // figure.addListener("pointerup", () => {
      //   deselectFigure();
      // });

      app.current!.stage.addChild(figure as any);
      figuresObjects.current[id] = figure;
    }

    return figure;
  };

  const isActionDone = (figure: FigureData): boolean => {
    if (!figure.activeMove) {
      return true;
    }
    const now: Moment = moment();

    return now.isAfter(figure.activeMove.moveEndAt);
  };

  const calculateFigureXY = (figure: FigureData): [number, number] => {
    if (!figure.activeMove) {
      return [figure.x, figure.y];
    }

    const now: Moment = moment();
    const startFromNowDiff = now.diff(
      figure.activeMove!.moveStartAt,
      "milliseconds"
    );
    const startFromEndDiff = figure.activeMove!.moveEndAt.diff(
      figure.activeMove!.moveStartAt,
      "milliseconds"
    );

    const currentX: number =
      figure.activeMove.moveStartX +
      (figure.activeMove!.moveDestinitionX - figure.activeMove.moveStartX) *
        (startFromNowDiff / startFromEndDiff);
    const currentY: number =
      figure.activeMove.moveStartY +
      (figure.activeMove!.moveDestinitionY - figure.activeMove.moveStartY) *
        (startFromNowDiff / startFromEndDiff);

    return [currentX, currentY];
  };

  const clearFiguresOnRestart = () => {
    if (status === "pending") {
      Object.keys(figuresObjects.current).forEach((key: string) => {
        app.current!.stage.removeChild(figuresObjects.current[key] as any);
      });
    }
  };

  const getPlayButtonText = (): string => {
    return status === "pause" || status === "pending" ? "Play" : "Pause";
  };

  const getShowPlayButton = (): boolean => {
    return status !== "over";
  };

  const getRestartButtonText = (): string => {
    return status === "over" ? "Restart" : "End Game";
  };

  const getShowRestartButton = (): boolean => {
    return status === "over" || status === "play";
  };

  useLayoutEffect(() => {
    app.current = new Application({
      backgroundColor: 0xeef1f5,
      width: 1000,
      height: 800,
    });
    
    app.current!.stage.eventMode = "dynamic";
    app.current!.stage.addEventListener("mousemove", (e: FederatedPointerEvent) => {
      moveSelectedFigure(e.screenX, e.screenY);
      // console.log('mousemove');
    });
    app.current!.stage.addEventListener("mouseup", (e: FederatedPointerEvent) => {
      deselectFigure();
      console.log("deselect");
    });
    app.current!.stage.addEventListener("mouseleave", (e: FederatedPointerEvent) => {
      deselectFigure();
      console.log("deselect");
    });

    const background = new Graphics();
    background.beginFill("blue", 0.1);
    background.drawRect(
      0,
      0,
      app.current!.screen.width,
      app.current!.screen.height
    );
    background.eventMode = "static";
    // background.addEventListener("mousemove", (e: FederatedPointerEvent) => {
    //   moveSelectedFigure(e.screenX, e.screenY);
    //   // console.log('mousemove');
    // });
    // background.addEventListener("mouseup", (e: FederatedPointerEvent) => {
    //   deselectFigure();
    //   console.log("mouseup");
    // });
    // background.addEventListener("mouseleave", (e: FederatedPointerEvent) => {
    //   deselectFigure();
    //   console.log("mouseleave");
    // });
    app.current!.stage.addChild(background as any);

    const water = new Graphics();
    water.beginFill("blue", 0.4);
    water.drawRect(
      0,
      app.current!.screen.height - WATER_HEIGHT,
      app.current!.screen.width,
      WATER_HEIGHT
    );
    background.addChild(water as any);

    const playButton = new Text(getPlayButtonText());
    playButton.x = 850;
    playButton.y = 50;
    playButton.eventMode = "static";
    playButton.cursor = "pointer";
    playButton.addListener("pointerdown", () => {
      status === "play" ? setPause() : setPlay();
    });
    background.addChild(playButton as any);

    const restartButton = new Text(getRestartButtonText());
    restartButton.x = 850;
    restartButton.y = 150;
    restartButton.eventMode = "static";
    restartButton.cursor = "pointer";
    restartButton.addListener("pointerdown", () => {
      status === "play" ? endGame() : setPending();
    });
    background.addChild(restartButton as any);

    gameField.current = new Graphics();
    gameField.current.beginFill("red", 0.1);
    gameField.current.drawRect(
      field.x,
      field.y,
      field.horizontalCellsCount * 40,
      field.verticalCellsCount * 40
    );
    background.addChild(gameField.current as any);

    app.current!.ticker.add((delta) => {
      // console.log(figures);

      playButton.text = getPlayButtonText();
      playButton.alpha = getShowPlayButton() ? 1 : 0;
      restartButton.text = getRestartButtonText();
      restartButton.alpha = getShowRestartButton() ? 1 : 0;

      field.cells.forEach((cellData: FiledCellData) => {
        const cell = getOrCreateCell(cellData);
      });

      clearFiguresOnRestart();

      if (status === "play") {
        figures.forEach((f) => {
          const figure = getOrCreateFigure(f.id);
          if (isActionDone(f)) {
            onActionDone(f.id);
          }
          const [currentX, currentY] = calculateFigureXY(f);
          f.x = currentX;
          f.y = currentY;
          figure.x = f.x;
          figure.y = f.y;
        });
      }
    });
    document.getElementById("game")!.appendChild(app.current!.view as any);
  }, []);

  return <div className="layout" id="game"></div>;
};
