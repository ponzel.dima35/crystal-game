import moment from "moment";
import { Moment } from "moment";
import uuid from "react-uuid";

const FIELD_WIDTH = 1000;
const FIELD_HEIGHT = 800;
const FIGURE_START_POSITION = [50, 250];
const FIGURE_MOVE_DOWN_SPEED = 145;
const GENERATE_FIGURE_INTERVAL = 2000;
export const WATER_HEIGHT = 200;
const SPACE_BETWEEN_FIGURES_IN_WATER = 60;
const TIME_TO_MOVE_IN_WATER_IN_SECCONDS = 0.5;
const TIME_TO_MOVE_TO_CELL_IN_SECCONDS = 0.5;
const LENGTH_TO_BE_NEAR = 20;

enum FigureState {
  Fall,
  Selected,
  InWater,
  MoveInWater,
  Set,
}

type FigureActiveMove = {
  moveStartAt: Moment;
  moveEndAt: Moment;
  moveStartX: number;
  moveStartY: number;
  moveDestinitionX: number;
  moveDestinitionY: number;
};

export type FigureData = {
  id: string;
  x: number;
  y: number;
  width: number;
  state: FigureState;
  activeMove: FigureActiveMove | null;
};

export type FiledCellData = {
  verticalPosition: number;
  horizontalPosition: number;
};

export type FieldData = {
  x: number;
  y: number;
  verticalCellsCount: number;
  horizontalCellsCount: number;
  cells: Array<FiledCellData>;
};

const fieldDefault: FieldData = {
  x: 100,
  y: 100,
  horizontalCellsCount: 3,
  verticalCellsCount: 3,
  cells: [
    { horizontalPosition: 1, verticalPosition: 1 },
    { horizontalPosition: 2, verticalPosition: 2 },
    { horizontalPosition: 3, verticalPosition: 3 },
  ],
};

let fieldWidth: number = FIELD_WIDTH;
let fieldHeight: number = FIELD_HEIGHT;
let onPauseDateTime: Moment | null = null;
export let field: FieldData = fieldDefault;
export let status: "pending" | "play" | "pause" | "over" = "pending";
export let figures: Array<FigureData> = [];
let figuresInWaterRef: Array<FigureData> = [];
let lastFigureInWaterPositionRef: number = 0;
let addFigureTimer: NodeJS.Timer | null = null;

const recalculateActiveMoveEndTime = (): void => {
  const timeDelta = moment().diff(onPauseDateTime, "milliseconds");
  figures.forEach((figure: FigureData) => {
    if (figure.activeMove) {
      figure.activeMove.moveEndAt.add(timeDelta, "milliseconds");
    }
  });
};

export const setPlay = () => {
  if (status === "pause") {
    recalculateActiveMoveEndTime();
  }
  status = "play";
  onGameStatusChange();
};

export const setPause = () => {
  status = "pause";
  onPauseDateTime = moment();
  onGameStatusChange();
};

export const endGame = () => {
  status = "over";
  onPauseDateTime = null;
  onGameStatusChange();
};

export const setPending = () => {
  status = "pending";
  onPauseDateTime = null;
  onGameStatusChange();
  figuresInWaterRef = [];
  figures = [];
  lastFigureInWaterPositionRef = 0;
};

const getFigureById = (id: string): FigureData | null => {
  return figures.find((figure: FigureData) => figure.id === id) ?? null;
};

const startFallFigure = (figure: FigureData): void => {
  const now: Moment = moment();
  const heightToFall: number = fieldHeight - WATER_HEIGHT - figure.y;
  const timeWhenFelt: Moment = now
    .clone()
    .add(heightToFall / FIGURE_MOVE_DOWN_SPEED, "seconds");

  figure.activeMove = {
    moveStartX: figure.x,
    moveStartY: figure.y,
    moveDestinitionX: figure.x,
    moveDestinitionY: fieldHeight - WATER_HEIGHT,
    moveStartAt: now,
    moveEndAt: timeWhenFelt,
  };
};

const startMoveFigureInWater = (
  figure: FigureData,
  x: number,
  y: number
): void => {
  const now: Moment = moment();
  const timeWhenMoved: Moment = now
    .clone()
    .add(TIME_TO_MOVE_IN_WATER_IN_SECCONDS, "seconds");

  figure.activeMove = {
    moveStartX: figure.x,
    moveStartY: figure.y,
    moveDestinitionX: x,
    moveDestinitionY: y,
    moveStartAt: now,
    moveEndAt: timeWhenMoved,
  };
};

const moveFiguresIfColizionInWater = (): void => {
  let currentLastFigurePositionX = 0;
  figuresInWaterRef.forEach((figure: FigureData) => {
    if (figure.x !== currentLastFigurePositionX) {
      figure.state = FigureState.MoveInWater;
      startMoveFigureInWater(
        figure,
        currentLastFigurePositionX + SPACE_BETWEEN_FIGURES_IN_WATER,
        fieldHeight - WATER_HEIGHT
      );

      currentLastFigurePositionX =
        figure.width +
        currentLastFigurePositionX +
        SPACE_BETWEEN_FIGURES_IN_WATER;
    }
  });

  lastFigureInWaterPositionRef = currentLastFigurePositionX;
  if (lastFigureInWaterPositionRef > FIELD_WIDTH) {
    endGame();
  }
};

const isFigureInWater = (figure: FigureData): boolean => {
  return figure.y >= fieldHeight - WATER_HEIGHT;
};

const isPointNearPoint = (
  x1: number,
  y1: number,
  x2: number,
  y2: number
): boolean => {
  return (
    Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2)) <= LENGTH_TO_BE_NEAR
  );
};

const findFigureNearCell = (figure: FigureData): FiledCellData | null => {
  let resultCell: FiledCellData | null = null;
  field.cells.forEach((cell: FiledCellData) => {
    const cellX: number = field.x + (cell.horizontalPosition - 1) * 40;
    const cellY: number = field.y + (cell.verticalPosition - 1) * 40;
    if (isPointNearPoint(cellX, cellY, figure.x, figure.y)) {
      resultCell = cell;
      return;
    }
  });

  return resultCell;
};

const isFigureNearCell = (figure: FigureData): boolean => {
  return !!findFigureNearCell(figure);
};

const startMoveFigureToCell = (figure: FigureData): void => {
  const now: Moment = moment();
  const timeWhenMoved: Moment = now
    .clone()
    .add(TIME_TO_MOVE_TO_CELL_IN_SECCONDS, "seconds");

  const cell: FiledCellData = findFigureNearCell(figure)!;

  figure.activeMove = {
    moveStartX: figure.x,
    moveStartY: figure.y,
    moveDestinitionX: field.x + (cell.horizontalPosition - 1) * 40,
    moveDestinitionY: field.y + (cell.verticalPosition - 1) * 40,
    moveStartAt: now,
    moveEndAt: timeWhenMoved,
  };
};

const generateFigure = () => {
  const lastFigureInWater = figuresInWaterRef.length
    ? figuresInWaterRef[figuresInWaterRef.length - 1]
    : null;

  const figureX: number = lastFigureInWater
    ? lastFigureInWater.x + SPACE_BETWEEN_FIGURES_IN_WATER
    : FIGURE_START_POSITION[0];

  const newFigure: FigureData = {
    id: uuid(),
    x: figureX,
    y: FIGURE_START_POSITION[1],
    width: 40,
    state: FigureState.Fall,
    activeMove: null,
  };

  figures.push(newFigure);

  startFallFigure(newFigure);
};

export const selectFigure = (id: string): void => {
  figuresInWaterRef = [
    ...figuresInWaterRef.filter((figure: FigureData) => figure.id !== id),
  ];

  const figure = getFigureById(id);
  if (figure) {
    figure.state = FigureState.Selected;
    figure.activeMove = null;
  }
};

export const deselectFigure = (): void => {
  const figure = figures.find(
    (figure: FigureData) => figure.state === FigureState.Selected
  );
  if (!figure) {
    return;
  }

  if (isFigureInWater(figure)) {
    figuresInWaterRef.push(figure);
    moveFiguresIfColizionInWater();
    figure.state = FigureState.MoveInWater;
  } else if (isFigureNearCell(figure)) {
    startMoveFigureToCell(figure);
    figure.state = FigureState.Set;
  } else {
    startFallFigure(figure);
    figure.state = FigureState.Fall;
  }
};

export const moveSelectedFigure = (x: number, y: number): void => {
  const selectedFigure = figures.find(
    (figure: FigureData) => figure.state === FigureState.Selected
  );

  const now: Moment = moment();

  if (selectedFigure) {
    // selectedFigure.activeMove = {
    //   moveStartX: selectedFigure.x,
    //   moveStartY: selectedFigure.y,
    //   moveDestinitionX: x,
    //   moveDestinitionY: y,
    //   moveStartAt: now,
    //   moveEndAt: now.clone().add(1, "milliseconds"),
    // };
    selectedFigure.x = x;
    selectedFigure.y = y;
  }
};

export const onActionDone = (id: string): void => {
  const figure = getFigureById(id);
  if (!figure) {
    return;
  }

  figure.activeMove = null;

  if (figure.state === FigureState.Fall) {
    figuresInWaterRef.push(figure);
    moveFiguresIfColizionInWater();
    figure.state = FigureState.MoveInWater;
  } else if (figure.state === FigureState.MoveInWater) {
    figure.state = FigureState.InWater;
  }
};

const onGameStatusChange = () => {
  if (status === "play") {
    generateFigure();

    addFigureTimer = setInterval(() => {
      generateFigure();
    }, GENERATE_FIGURE_INTERVAL);
  } else {
    if (addFigureTimer) {
      clearInterval(addFigureTimer);
    }
  }
};
