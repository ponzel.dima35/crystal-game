import { createBrowserRouter } from "react-router-dom";
import { GamePage } from "./pages";

export const Router = createBrowserRouter([
  {
    path: "/",
    element: <GamePage/>,
  },
]);
